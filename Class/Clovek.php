<?php

abstract class Clovek
{

    private $jmeno;
    private $prijmeni;

    public $vek = 0;

    protected $plat;

    public function __construct($jmeno, $prijmeni)
    {
        $this->jmeno = $jmeno;
        $this->prijmeni = $prijmeni;
    }

    public function getJmeno()
    {
        return $this->jmeno;
    }

    public function getPlat()
    {
        return $this->plat;
    }

    public function setPlat($value)
    {
        $this->plat = $this->spocitejCistouMzdu($value);
        return $this;
    }
    
    public function getClovekData()
    {
        return [
            'jmeno' => $this->jmeno,
            'prijmeni' => $this->prijmeni,
            'vek' => $this->vek
        ];
    }

    private function spocitejCistouMzdu($value)
    {
        return $value * 0.85;
    }
    
    public abstract function vypoctiMzdu();
    
}
