<?php

class Student extends Clovek implements IClovek
{
    
    private $fakulta;
    
    public function __construct($jmeno, $prijmeni, $fakulta) 
    {
        parent::__construct($jmeno, $prijmeni);
        $this->fakulta = $fakulta;
    }
    
    public function getFakulta()
    {
        return $this->fakulta;
    }
    
    public function getClovekData() 
    {
        $arr = parent::getClovekData();
        $arr['fakulta'] = $this->fakulta;
        return $arr;
    }

    public function vypoctiMzdu() 
    {
        return $this->plat * 2 - (100 / 50 * 0.6);
    }

    public function vypoctiHrubouMzdu() 
    {
        return $this->vypoctiMzdu() * 1.15;
    }

}
