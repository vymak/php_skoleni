<?php

class Duchodce extends Clovek implements IClovek
{

    public function vypoctiMzdu() 
    {
        return $this->plat * 0.4;
    }

    public function vypoctiHrubouMzdu() 
    {
        return $this->vypoctiMzdu() * 1.15;
    }

}
