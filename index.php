<?php

require 'vendor/autoload.php';

// set error reporting
error_reporting(E_ALL);

$robotLoader = new \Nette\Loaders\RobotLoader();
$robotLoader->addDirectory('Class')
        ->addDirectory('Interface')
        ->setTempDirectory('temp')
        ->register();

/*$jarda = new Clovek('Jarda', 'Suchý');
$jarda->vek = 25;
echo $jarda->getJmeno();
var_dump($jarda->getClovekData());

echo $jarda->setPlat(20000)
        ->getPlat();
 */

$luca = new Student('Lucík', 'Sirotková', 'FIS');
$luca->vek = 18;
$luca->setPlat(100000);

$libor = new Duchodce('Libor', 'Vymětalík');
$libor->setPlat(20000);

$people = [$luca, $libor];
foreach ($people as $item) {
    var_dump($item->vypoctiMzdu());
}

vratHrubouMzdu($luca);
vratHrubouMzdu($libor);

function vratHrubouMzdu(IClovek $clovek) {
    var_dump($clovek->vypoctiHrubouMzdu());
}

/*echo $jarda->vek;
$jarda->vek = 18;
echo $jarda->vek;*/

/*$zdenda = new Clovek('Zdeněk', 'Košárek');
$zdenda->getJmeno();*/